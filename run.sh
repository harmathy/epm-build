#!/bin/bash

set -e

# check for Makefile
if ! [ -f /src/Makefile ]; then
  echo "There is no Makefile in your package directory."
  exit 1
fi

# work on a copy of the packaging directory
cp -r /src /build/src
cd /build/src

# clean
git clean -xdfq

# build via make
make

# copy artifacts over to the package directory
chown "$(stat -c '%u:%g' /src/Makefile)" ./*.deb
cp ./*.deb /pkg

