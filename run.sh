#!/bin/bash

# epm-build
# Copyright (C) 2022 Max Harmathy <harmathy@mailbox.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

# check for Makefile
if ! [ -f /src/Makefile ]; then
  echo "There is no Makefile in your package directory."
  exit 1
fi

# work on a copy of the packaging directory
cp -r /src /build/src
cd /build/src

# clean
git clean -xdfq

# build via make
make

# copy artifacts over to the package directory
chown "$(stat -c '%u:%g' /src/Makefile)" ./*.deb
cp ./*.deb /pkg
