epm-build
=========

Container for building packages with make and epm.

Usage
-----

Build the image for the container:

```
./build.sh
```

The epm build script running inside the container
* expects the source directory in `/src` and
* puts the generated packages in `/pkg`.

We can run the build by:

```
podman run --rm -v $PWD:/src -v $PWD:/pkg epm-build
```

In this example we change into the directory of the package to build and let
the build process put the resulting packages in the same directory. Replace
`$PWD` with the path you need, if desired.

Requisites
----------

The build script expects
* the source directory to be a non-bare git repository and
* a Makefile in this directory to build packages.
