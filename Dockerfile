FROM debian:buster

ENV LANG=C.UTF-8 \
    DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /usr/share/man/man1 \
    && apt-get update \
    && apt-get -qy upgrade \
    && apt-get -qy dist-upgrade \
    && apt-get -qy install --no-install-recommends \
        git \
        build-essential \
        epm \
        sudo \
    && apt-get -qy autoremove --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY run.sh /build/run.sh

CMD /build/run.sh
