#!/bin/bash

# epm-build
# Copyright (C) 2022 Max Harmathy <harmathy@mailbox.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

working_container=$(buildah from --pull docker.io/library/debian:buster)

buildah config --env LANG=C.UTF-8 --env DEBIAN_FRONTEND=noninteractive "$working_container"

packages_to_install=(
  build-essential
  epm
  git
  man
  sudo
)

buildah run "$working_container" -- apt-get update
buildah run "$working_container" -- apt-get -qy dist-upgrade
buildah run "$working_container" -- apt-get -qy install --no-install-recommends "${packages_to_install[@]}"
buildah run "$working_container" -- apt-get -qy autoremove --purge
buildah run "$working_container" -- apt-get clean
buildah run "$working_container" -- rm -rf /var/lib/apt/lists/*

buildah copy "$working_container" run.sh /build/run.sh

buildah config --cmd /build/run.sh "$working_container"

buildah commit --unsetenv LANG --unsetenv DEBIAN_FRONTEND --rm "$working_container" epm-build
